# Stimulus Tests

Stepping through the [The Stimulus Handbook](https://stimulusjs.org/handbook/introduction) and learning [Stimulus](https://github.com/stimulusjs/stimulus). 

---

You can [remix `stimulus-starter` on Glitch](https://glitch.com/edit/#!/import/github/stimulusjs/stimulus-starter) so you can work entirely in your browser without installing anything.

[![Remix on Glitch](https://cdn.glitch.com/2703baf2-b643-4da7-ab91-7ee2a2d00b5b%2Fremix-button.svg)](https://glitch.com/edit/#!/import/github/stimulusjs/stimulus-starter)

Happy experimenting!

