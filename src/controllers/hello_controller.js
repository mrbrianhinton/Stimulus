// Stimulus' purpose is to automatically connect DOM elements to JavaScript objects. Those objects are called controllers
import { Controller } from "stimulus"

export default class extends Controller {
  static targets = [ "name" ]

  greet() {
    console.log(`Hello, ${this.name}!`)
  }

  get name() {
    return this.nameTarget.value
  }

}
