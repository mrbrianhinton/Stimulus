// src/controllers/clipboard_controller.js
import { Controller } from "stimulus"



export default class extends Controller {
  static targets = [ "source" ]
  
  // hide the copy button if JavaScript isn't enabled
  connect() { 
    if (document.queryCommandSupported("copy")) {
      this.element.classList.add("clipboard--supported")
    }
  }
  
  // copy input value
  copy() {
    event.preventDefault()
    this.sourceTarget.select()
    document.execCommand("copy")
  }
}