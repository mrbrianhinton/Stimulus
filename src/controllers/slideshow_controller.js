// src/controllers/slideshow_controller.js
import { Controller } from "stimulus"

export default class extends Controller {
  static targets = [ "slide" ]
  
  // Stimulus lifecycle callback methods, and they are useful for setting up or tearing down associated state when your controller enters or leaves a document
  initialize() {
    this.showSlide(0)
  }

  next() {
    this.showSlide(this.index + 1)
  }

  previous() {
    this.showSlide(this.index - 1)
  }
  // method that loops over each slide target, toggling the slide--current class if it's index matches
  // the controller tracks it's state in the this.index property
  showSlide(index) {
    this.index = index
    this.slideTargets.forEach((el, i) => {
      el.classList.toggle("slide--current", index == i)
    })
  }
}